from itertools import count
from re import S
from Bio import SeqIO
from Bio.SeqUtils import GC
import pandas as pd
import os
import collections
import math
from scipy.stats import entropy


def fastq_read_num(fp_fastq):
    """Calculate entropy for a sequence of numbers
 
    Args:
        sequence : contig sequence
 
    Returns:
        float: entropy
 
    """
    sample_names = []
    read_num_fq=[]


    for fp in fp_fastq:

        folder_path = os.path.dirname(fp)
        folder = os.path.basename(folder_path)
        sample_names.append(folder)
        
        with open(fp, 'r') as f:

            count=0
            for line in f:
                if line.startswith("@"):
                    count+=1
                
            read_num_fq.append(count)



    return read_num_fq,sample_names