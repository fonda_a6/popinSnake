from re import S
from Bio import SeqIO
from Bio.SeqUtils import GC
import pandas as pd
import numpy as np
import os
import collections
import math
from scipy.stats import entropy



def cal_entropy(sequence):
    """Calculate entropy for a sequence of numbers
 
    Args:
        sequence : contig sequence
 
    Returns:
        float: entropy
 
    """
    d = dict()
    counted = 0

    for base_i in range(len(sequence)-1):
        dimer = sequence[base_i:base_i+2]
        if (dimer[0]!='N' and dimer[1]!='N'):
            # if dimer is new to dict
            if not dimer in d:
                d[dimer] = 1
                counted+=1
            # otherwise
            else:
                d[dimer] += 1
                counted+=1

    # calculate the entropy for dinucleotide counts
    entropy = 0
    for key, value in d.items():
        if value == 0:
            continue
        p = float(value) / counted
        entropy -= p * math.log(p) / math.log(2)

    return entropy / 4


def cal_N50(p):
    """Calculate N50 for a sequence of numbers
 
    Args:
        p : filepath
        len_of_sequences (list): List of sequence lengths
 
    Returns:
        float: median, N50 value.
        float: contigNum, number of contigs in the sample
 
    """
    seq_lengths=[]

    for seq_record in SeqIO.parse(p,'fasta'):  
        length=len(seq_record.seq)
        seq_lengths.append(length)
        contigNum = len(seq_lengths)


    all_len=sorted(seq_lengths, reverse=True)
    csum=np.cumsum(all_len)

    # n = int(sum(seq_lengths))
    n2=int(sum(seq_lengths)/2)

    # get index for cumsum >= N/2
    csumn2=min(csum[csum >= n2])
    ind=np.where(csum == csumn2)

    N50 = all_len[int(ind[0])]

 
    return N50, contigNum


def cal_gctable(p):

    """Calculate total GC percentage for assembled fasta file
 
    Args:
        p : the full filepath
 
    Returns:
        float: total GC% for the whole assambled sample
 
    """
    A=[]
    C=[]
    G=[]
    T=[]

    
    for seq_record in SeqIO.parse(p,'fasta'):
        seq = repr(seq_record.seq)
        A += [seq.count("A")]
        C += [seq.count("C")]
        G += [seq.count("G")]
        T += [seq.count("T")]

    A_T=sum(A)+sum(T)
    G_C=sum(G)+sum(C)

    sample_gc = round(G_C/(A_T + G_C)*100)

    return sample_gc



