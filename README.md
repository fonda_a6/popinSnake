**This repository is no longer maintained. 
Please go to the [popinSnake github repository](https://github.com/kehrlab/PopinSnake.git) for the updated and maintained workflow.**

# PopinSnake workflow

This Git repository contains a generic [Snakemake](https://snakemake.readthedocs.io/en/stable/) workflow for running the program [*PopIns4Snake*](https://gitlab.informatik.hu-berlin.de/fonda_a6/popins4snake.git).
It is supposed to facilitate your PopIns run and provides examples for a streamlined workflow. 

## Contents

1. [Prerequisites](#prerequisites)
1. [Download and installation](#download-and-installation)
1. [Setting up your workflow](#setting-up-your-workflow)
1. [Workflow execution](#workflow-execution)
1. [Example data](#example-data)
1. [References](#references)


## Prerequisites

As a minimum, an installation of the [Conda package manager](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html) (Miniconda) is required for running the workflow.
In addition, we recommend to install [Mamba](https://mamba.readthedocs.io/en/latest/installation.html) in the Conda base environment:

```
$ conda install mamba -n base -c conda-forge
```

If you do *not* have [Snakemake](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html) installed already, create and activate a Conda environment for it:

```
$ conda activate base
$ mamba create -c conda-forge -c bioconda -n snakemake snakemake
$ conda activate snakemake
```

Most dependencies of the PopIns workflow will be automatically installed via Conda/Mamba when running the workflow for the first time.

Dependencies that are not available in Conda are included as submodules in this repository and need to be installed before you can run the workflow.
These installations require a C++14 capable compiler, e.g. [GCC](https://gcc.gnu.org) version &ge;4.9.2, [CMake](https://cmake.org) version &ge;2.8.12 and [Zlib](http://www.zlib.net/) to be available on your system.
If *not* already available on your system, they can be installed via Conda/Mamba:

```
$ conda activate base
$ mamba create -c conda-forge -n gcc cxx-compiler zlib cmake
$ conda activate gcc
```

## Download and installation

Download the PopinSnake workflow from this repository via

```
$ git clone --recursive https://gitlab.informatik.hu-berlin.de/fonda_a6/popinSnake.git
$ cd popinSnake
```

The flag `--recursive` is required to download the submodules.
Your initial folder structure should then look similar to this:

```
$ tree .
.
├── READFILE.md
├── README.md
├── example_data
│   ├── S0001
│   │   ├── S0001.bam
│   │   └── S0001.bam.bai
│   ├── S0002
│   │   ├── S0002.bam
│   │   └── S0002.bam.bai
│   ├── S0003
│   │   ├── S0003.bam
│   │   └── S0003.bam.bai
│   ├── chr21_ins.fa
│   ├── chr21_ins.fa.fai
│   └── virus_ref
│       ├── altref_virus_clean.fa
│       └── ...
├── submodules
│   ├── gatb-minia-pipeline
│   │   └── ...
│   ├── popins4snake
│   │   └── ...
│   └── sickle
│       └── ...
└── workflow
    ├── Snakefile
    ├── envs
    │   ├── bcftools.yml
    │   ├── bwa.yml
    │   ├── notebooks.yml
    │   ├── py27.yml
    │   ├── samtools.yml
    │   └── velvet.yml
    ├── notebooks
    │   ├── content_distribution.py.ipynb
    │   ├── contig_coverage_heatmap.py.ipynb
    │   ├── read_numbers_in_bar_graph.py.ipynb
    │   ├── remapped_comparison_053122.ipynb
    │   ├── sample_analysis_table.py.ipynb
    │   └── src
    │       └── stat_func
    │           ├── __init__.py
    │           ├── calculator.py
    │           └── visualization.py
    ├── snake_config.yaml
    └── subworkflow_remapping
        └── Snakefile
```

Before you can run the workflow, you need to install the submodules `popins4snake` and `sickle`.
The `gatb-minia-pipeline` submodule requires no separate installation.

#### Installing the *PopIns4Snake* submodule

[*PopIns4Snake*](https://gitlab.informatik.hu-berlin.de/fonda_a6/popins4snake) is the main program executed by the workflow.
Before we can compile *PopIns4Snake*, we need to compile and install its dependency [*Bifrost*](https://github.com/pmelsted/bifrost).
For this, navigate to the `bifrost` folder and compile *Bifrost* using the flag `MAX_KMER_SIZE=64`:

```
$ cd submodules/popins4snake/external/bifrost
$ mkdir local
$ mkdir build && cd build
$ cmake .. -DCMAKE_INSTALL_PREFIX=../local -DMAX_KMER_SIZE=64
$ make
$ make install
```

This installs *Bifrost* in the folder `submodules/popins4snake/external/bifrost/local`, which is where the *PopIns4Snake* Makefile will look for it.

Now we can go back to the main `popins4snake` folder and compile *PopIns4Snake*:

```
$ cd ../../../
$ mkdir build
$ make
```

A binary `popins4snake` should now be available in `submodules/popins4snake/`.
The workflow is pre-configured to search for it in this folder.


#### Installing the *Sickle* submodule

[*Sickle*](https://github.com/najoshi/sickle) is a small program for filtering and trimming sequencing reads.
All you need to do to install *Sickle* is to navigate to its folder and run make:

```
$ cd submodules/sickle
$ make
```

This should create the binary `sickle` in `submodules/sickle/`.
It is not required to copy the binary to a location available in your path as described in the *Sickle* installation instructions.
The workflow is pre-configured to search for the `sickle` binary in the `submodules/sickle/` folder.

_If running into environment issue while compiling `sickle`, try_
```
$ make CFLAGS+="-I${CONDA_PREFIX}/include"
```

</br>

In case you created the GCC Conda environment, remember to now activate the Snakemake environment again:

```
$ conda activate snakemake
```

## Setting up your workflow

You can find all configurable parameters of the workflow in the file `workflow/snake_config.yaml`.
You will need to adjust them according to your input data, preferred output folders, etc. as described in the following.

The pre-configured values allow running the workflow on the [example data](#example-data) included in this repository.

#### Configuring locations of the input data

Input to the workflow is a set of BAM files along with BAM index (BAI) files, and the corresponding reference genome in FASTA format.
Optionally, an "alternative reference" in FASTA format for contamination removal can be specified (see also [Workflow behaviour](#configuring-workflow-behaviour)).

```
# Define input directories here
REFERENCE:
   ../path/to/hg38.fa
ALTREF:
   ../path/to/altref.fa
INPUT_DIR:
   ../path/to/my_bam_files/
```

Further, the workflow requires you to specify all sample names:

```
# Provide sample names here
SAMPLES:
   - genome1
   - genome2
   - genome3
```

Please make sure the list is consistent with your BAM files in the `INPUT_DIR`.
The workflow expects a separate subfolder for each sample (input BAM file) in the `INPUT_DIR`, hence, your folder structure for the input BAM files should look similar to:

```
INPUT_DIR
├── genome1
│   ├── genome1.bam
│   └── genome1.bam.bai
├── genome2
│   ├── genome2.bam
│   └── genome2.bam.bai
└── genome3
    ├── genome3.bam
    └── genome3.bam.bai
```

#### Configuring preferred output locations

The workflow creates two output folders in locations that you may specify:

```
# Define output directories here
WORK_DIR:
   ../workdir
RESULTS_DIR:
   ../results
```

The `WORK_DIR` is intended for intermediate data per sample, i.e., the workflow will create a subfolder per sample in this directory.
Depending on the number of BAM files input to the workflow, the space requirements of this folder may grow quite large.
You may specify it in a temporary location or choose to delete it after successful completion of the workflow.

The `RESULTS_DIR` will hold the final VCF and FASTA output files describing the called insertions as well as the figures created during intermediate data analyses.
You probably want to keep most files written to this folder.

#### Configuring workflow behaviour

You can influence the behaviour of the workflow by changing the parameters in this section

```
# Define workflow behaviour here
ASSEMBLER:
   "minia"
REMAP:
   "no"
```

The `ASSEMBLER` parameter specifies the program to use for assembling reads without alignment to the reference genome into contiguous sequences (contigs).
The default workflow installation supports two different assembly programs, [`"velvet"`](https://doi.org/10.1101/gr.074492.107) and [`"minia"`](https://doi.org/10.1186/1748-7188-8-22). 

The `REMAP` parameter can be used to activate a sub-workflow for remapping unaligned reads to the alternative reference (specified in the [input section](#configuring-locations-of-the-input-data) as `ALTREF`) by setting it to `REMAP="yes"`.
The purpose of the sub-workflow is to remove potential contamination in your read data.
More specifically, after cropping unaligned reads from the input BAM files, the remapping sub-workflow will attemt to align the unaligned reads to the alternative reference that consists of e.g. microbial genomes.
Any reads with an alignment to a sequence in the alternative reference will not be passed on to the following assembly steps.


#### Configuring workflow parameters

The main parameters of the programs called by the workflow can be configured in the section starting with

```
# Define other workflow parameter values here
```

In most cases you will not need to change the default values.
Available parameters include:

- `memory`: The maximum amount of memory `samtools sort` is allowed to use per thread. Default: `2G`.
- `threads`: The number of threads allowed for parallelizing rules calling `samtools sort`, `bwa mem`, `minia`, and `popins4snake merge-contigs`. Default: `16`.
- `kmerlength`: The k-mer length for Velvet assembly. Default: `29`.
- `k_merge`: The k-mer length for merging contigs across samples using `popins4snake merge-contigs`. Default: `63`.
- `readlen`: The read length required by `popins4snake place-refalign` and `popins4snake place-splitalign`. Default: `150`.

#### Configuring alternative installation directories

If you strictly followed the installation instructions above, you will not need to change the preconfigured values in this section.
If you install any of the submodules in another location, it is possible to change the default paths to the submodules' binaries:

```
# Define paths to binaries:
# (Change only if you are not using the default installation locations.)
POPINS2_BIN:
   path/to/popins4snake
MINIA_BIN:
   path/to/gatb-minia-pipeline/gatb
SICKLE_BIN:
   path/to/submodules/sickle/sickle
```


## Workflow Execution

After the initial setup, you can execute the workflow from the directory `workflow/` which contains the main Snakefile:

```
$ cd workflow/
$ snakemake --use-conda --cores 1
```

The tag `--use-conda` is required for successful workflow execution unless you have all workflow dependencies (and their correct versions) specified in Conda environments (see `workflow/envs/` folder) installed in a location available on your path.

__Note: When you run the workflow for the first time using `--use-conda`, it will create the conda environments.
This process may take some time. The next time you run the workflow, this process is not necessary again.__

If you chose not to install `mamba` (see [Prerequisites](#prerequisites)), then you now have to add `--conda-frontend conda` to the snakemake command.

Similarly, other Snakemake options are available.
For example, you can specify `--cores all` to use all available cores instead of just a single core or specify any other number of cores.

The Snakemake command includes execution of all Jupyter notebooks we implemented for intermediate data analysis.
The output of these analyses can be found in the `RESULTS_DIR` folder.

## Example data

We provide an `example_data` folder containing examplary data generated for testing purposes.
It also shows you how to prepare your data for the workflow.
The `snake_config.yaml` file is preconfigured to run the workflow on the example data.

You can simply navigate to the `workflow` folder and execute Snakemake to run the workflow on this data (see also [Workflow execution](#workflow-execution)):
```
$ cd workflow/
$ snakemake --use-conda --cores 1
```

__Note: When you run the workflow for the first time using `--use-conda`, it will create the conda environments.
This process may take some time. The next time you run the workflow, this process is not necessary again.__

In this example, the FASTA file `chr21_ins.fa` is used as a reference genome.
A subfolder for each sample is present and its name matches the BAM file name.
You can find three BAM files (`.bam`) and the corresponding index files (`.bam.bai`) in the three sample folders `S0001`, `S0002`, `S0003`.

In addition, a BWA-indexed alternative reference for contamination removal is provided in the subfolder `example_data/virus_ref/`.
However, the default configuration will not use it as the `REMAP` parameter is set to `"no"`.
Change the configuration to `REMAP="yes"` in order to test the remapping sub-workflow on the example data.


## References

Cao K., Elfaramawy N., Weidlich M., Kehr B. (2022)
__From Program Chains to Exploratory Workflows: PopinSnake for Insertion Detection in Genomics.__
In: 2023 IEEE 19th International Conference on e-Science (e-Science).

Krannich T., White W. T. J., Niehus S., Holley G., Halldórsson B. V., Kehr B. (2022)
__Population-scale detection of non-reference sequence variants using colored de Bruijn graphs.__
[Bioinformatics, 38(3):604–611](https://academic.oup.com/bioinformatics/article/38/3/604/6415820).

Kehr B., Helgadóttir A., Melsted P., Jónsson H., Helgason H., Jónasdóttir Að., Jónasdóttir As.,	Sigurðsson Á., Gylfason A., Halldórsson G. H., Kristmundsdóttir S., Þorgeirsson G., Ólafsson Í., Holm H., Þorsteinsdóttir U., Sulem P., Helgason A., Guðbjartsson D. F., Halldórsson B. V., Stefánsson K. (2017).
__Diversity in non-repetitive human sequences not found in the reference genome.__
[Nature Genetics,](http://rdcu.be/pDbJ) [49(4):588–593](https://www.nature.com/articles/ng.3801).

Kehr B., Melsted P., Halldórsson B. V. (2016).
__PopIns: population-scale detection of novel sequence insertions.__
[Bioinformatics, 32(7):961-967](https://academic.oup.com/bioinformatics/article/32/7/961/2240308).
